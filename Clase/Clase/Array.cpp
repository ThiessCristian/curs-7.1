#include "Array.h"



Array::Array()
{}

Array::Array(int size, int * arr):size(size)
{
   arrayValues = new int[size];
   for (int i = 0; i < size; i++) {
      arrayValues[i] = arr[i];
   }
}

Array::Array(const Array & arr)
{
   size = arr.size;
   arrayValues = new int[size];

   for (int i = 0; i < size; i++) {
      arrayValues[i] = arr.arrayValues[i];
   }
   
}
Array::~Array()
{}
