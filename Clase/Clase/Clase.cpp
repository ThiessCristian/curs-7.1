#include <iostream>

class Test
{
   int x;

public:
   int getX() { return x; }
   void setX(int x){this->x = x;}

};

void main()
{
   Test t;
   t.setX(10);
   std::cout << t.getX();
}