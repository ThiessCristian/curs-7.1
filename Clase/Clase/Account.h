#pragma once
#include <fstream>
#include <string>

class Account
{
private:
   int id;
   double balance=0.0;
   std::string filename;

   void setFilename();

public:
   Account();
   Account(int id,double balance);
   ~Account();

   void withdraw(const double& ammount);
   void deposit(const double& ammount);
   double getBalance();



};

