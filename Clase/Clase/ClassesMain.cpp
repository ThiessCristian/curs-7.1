#include "Account.h"
#include "Array.h"
#include <iostream>

class Point
{
public:
   Point()
   {
      std::cout << "constructor";
   }

   Point(const Point&t)
   {
      std::cout << "Copy";
   }
};



int main()
{/*
   Account a(1,5);
   Account b(2, 23);
   
   a.deposit(1);
   a.withdraw(2);
   std::cout<<a.getBalance();
   
   b.deposit(2);
   b.withdraw(2000);
*/

/*
   int values[3] = {1,2,3};
   Array arr(3, values);
   Array copyArray(arr);


   arr.setFirstValue(2);
*/

   Point *t1, *t2;
   t1 = new Point();
   t2 = new Point(*t1);
   Point t3 = *t1;
   Point t4;
   t4 = t3;


   return 0;
}