#include "Account.h"



void Account::setFilename()
{
   std::string filename = "account" + std::to_string(id) + ".txt";
}

Account::Account()
{
   
}

Account::Account(int id,double balance):id(id),balance(balance)
{  
   std::ofstream file(filename);

   file << "Account"<<id<< "created with:" << balance<<'\n';
   file.close();
}

Account::~Account()
{}

void Account::withdraw(const double & ammount)
{
   std::ofstream file(filename,std::ios_base::app);

   file << "removed:" << ammount << '\n';
   this->balance -= ammount;

   file << "total:" << balance << '\n';
   file.close();
}

void Account::deposit(const double & ammount)
{
   std::ofstream file(filename, std::ios_base::app);

   file << "added:" << ammount << '\n';
   this->balance += ammount;

   file << "total:" << balance << '\n';
   file.close();
}

double Account::getBalance()
{
   return balance;
}
