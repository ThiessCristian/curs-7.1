#pragma once

#include <string>

class Address
{
private:
   std::string city;
   std::string state;
   size_t number;

public:
   Address();
   Address(std::string city, std::string state, size_t number) :city(city), state(state), number(number) {};
   ~Address();

   std::string getCity() { return city; };
   void setCity(std::string city) { this->city = city; };

   std::string getState() { return state; };
   void setState(std::string state) { this->state = state; };

   size_t getNumber() { return number; };
   void setNumber(size_t number) { this->number = number; };

   friend std::istream& operator>>(std::istream &input, Address& address);
   friend std::ostream& operator<<(std::ostream &output, const Address& address);

};

