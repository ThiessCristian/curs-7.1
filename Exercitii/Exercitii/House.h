#pragma once
#include "Address.h"
#include "Room.h"
#include <iostream>


class House
{
private:
   std::string name;
   Address address;
   Room* rooms;
   size_t nrRooms;

public:
   House();
   House(const House& house);
   ~House();

   friend std::istream& operator>>(std::istream &input, House& house);
   friend std::ostream& operator<<(std::ostream &output, const House& house);

};

