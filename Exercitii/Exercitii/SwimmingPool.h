#pragma once
class SwimmingPool
{
   double length; //m 
   double width; // m
   double depth; // m
   double fillingRate; //l/min
   double drainingRate; //l/min
   double currentWater; //l

public:
   SwimmingPool();
   ~SwimmingPool();

   double getAmountOfWaterForFill();
   double getTimeToFill();
   void addWater();
   void drainWater();

};

