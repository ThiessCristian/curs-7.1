#include "Address.h"
#include <iostream>


Address::Address()
{}


Address::~Address()
{}

std::istream & operator>>(std::istream & input, Address & address)
{
   std::cout << "state:";
   input >> address.state;
   std::cout << "city:";
   input >> address.city;
   std::cout << "number:";
   input>> address.number;
   return input;
}

std::ostream & operator<<(std::ostream & output, const Address & address)
{

   output <<"State: "<< address.state<<" City: "<< address.city<<" Nr: "<< address.number<<'\n';
   return output;
}
