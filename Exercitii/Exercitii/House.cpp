#include "House.h"
#include "Room.h"


House::House()
{}

House::House(const House & house)
{
   name = house.name;
   nrRooms = house.nrRooms;

   rooms = new Room[nrRooms];

   for (size_t i = 0; i < nrRooms; i++) {
      rooms[i] = house.nrRooms[i];
   }
   address = house.address;
}


House::~House()
{
   delete[] rooms;
}

std::istream & operator>>(std::istream & input, House & house)
{
   std::cout << "name:";
   input >> house.name;
   std::cout << "nr of rooms:";
   input >> house.nrRooms;

   house.rooms = new Room[house.nrRooms];

   for (size_t i = 0; i < house.nrRooms; i++) {
      input >> house.rooms[i];
   }
   std::cout << "address:"<<'\n';
   input >> house.address;

   return input;
}

std::ostream & operator<<(std::ostream & output, const House & house)
{
   output << house.name<<'\n';
   for (int i = 0; i < house.nrRooms; i++) {
      output << "Room " << i<<": ";
      output << house.rooms[i];
   }
   output << house.address;
   return output;
}
