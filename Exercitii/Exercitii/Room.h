#pragma once
#include <iostream>

class Room
{
private:
   double length;
   double breadth;
   double height;

public:
   Room();
   Room(double length, double breadth, double height) :length(length), breadth(breadth), height(height) {};
   ~Room();

   double getLength() { return length; }
   void setLength(double length) { this->length = length; }

   double getBreadth() { return breadth; }
   void setBreadth(double breadth) { this->breadth = breadth; }

   double getHeight() { return height; }
   void setHeigth(double height) { this->height = height; }

   friend std::istream& operator>>(std::istream &input, Room& room);
   friend std::ostream& operator<<(std::ostream &output, const Room& room);
};

